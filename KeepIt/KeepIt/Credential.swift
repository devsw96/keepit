//
//  Credential.swift
//  KeepIt
//
//  Created by IMFORM-MM-2122 on 2022/10/26.
//
//

import Foundation
import RealmSwift

class Credential: Object, ObjectKeyIdentifiable {
    @Persisted(primaryKey: true) var id: ObjectId
    @Persisted var created: String
    @Persisted var title: String
    @Persisted var userName: String
    @Persisted var passwordKey: String
    @Persisted var memo: String?
    @Persisted(originProperty: "credentials") var credentialGroup: LinkingObjects<CredentialGroup>
    
    convenience init(
        title: String,
        userName: String,
        passwordKey: String,
        memo: String?
    ) {
        self.init()
        self.title = title
        self.userName = userName
        self.passwordKey = passwordKey
        self.memo = memo
        self.created = Date().toFormat(dateFormat: "yyyy-MM-dd")
    }
}

class CredentialGroup: Object, ObjectKeyIdentifiable {
    @Persisted(primaryKey: true) var id: ObjectId
    @Persisted var title: String
    @Persisted var sequence: Int
    @Persisted var credentials: RealmSwift.List<Credential>
    
    convenience init(
        title: String,
        sequence: Int,
        credentials: RealmSwift.List<Credential> = RealmSwift.List<Credential>()
    ) {
        self.init()
        self.title = title
        self.sequence = sequence
        self.credentials = credentials
    }
}


//class Credential: Object {
//    @Persisted(primaryKey: true) var id: ObjectId
//    @Persisted var created: String
//    @Persisted var title: String
//    @Persisted var userName: String
//    @Persisted var passwordKey: String
//    @Persisted var memo: String?
//
//    let group = LinkingObjects(fromType: CredentialGroup.self, property: "credentials")
//
//    convenience init(
//        title: String,
//        userName: String,
//        passwordKey: String,
//        memo: String?
//    ) {
//        self.init()
//        self.title = title
//        self.userName = userName
//        self.passwordKey = passwordKey
//        self.memo = memo
//        self.created = Date().toFormat(dateFormat: "yyyy-MM-dd")
//    }
//}
//
//class CredentialGroup: Object {
//    @Persisted(primaryKey: true) var id: ObjectId
//    @Persisted var title: String
//    @Persisted var credentials: List<Credential>
//    @Persisted var sequence: Int = 0
//
//    convenience init(
//        title: String,
//        credentials: List<Credential>,
//        sequence: Int
//    ) {
//        self.init()
//        self.title = title
//        self.credentials = credentials
//    }
//}
