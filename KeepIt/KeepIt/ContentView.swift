//
//  ContentView.swift
//  KeepIt
//
//  Created by IMFORM-MM-2122 on 2022/10/26.
//

import SwiftUI
import RealmSwift
import Realm

enum CredentialDisplayType: CaseIterable {
    case SINGLE
    case GROUPED
    
    var title: String {
        switch self {
        case .SINGLE:
            return "전체"
        case .GROUPED:
            return "그룹"
        }
    }
}

struct ContentView: View {
    @ObservedResults(CredentialGroup.self) var credentailGroup
    @ObservedResults(Credential.self) var credential
    @State var segmentType: CredentialDisplayType = .SINGLE
    @State var userName: String = ""
    @State var password: String = ""

    var body: some View {
        VStack {
            Picker("", selection: $segmentType) {
                ForEach(CredentialDisplayType.allCases, id: \.self) {
                    Text($0.title)
                }
            }
            .pickerStyle(.segmented)

            switch segmentType {
            case .SINGLE :
                if let firstGroup = credentailGroup.first {
                    CredentialListView(credentialGroup: firstGroup)
                }
            case .GROUPED:
                TextField("Enter Your Email", text: $userName)
                    .textContentType(.emailAddress)
                    .keyboardType(.emailAddress)
                    .padding([.horizontal])

                SecureField("Enter Your Password", text: $password)
                    .textContentType(.password)
                    .padding([.horizontal])
                
                Spacer()
            }
            

        }
        .onAppear {
            print(Realm.Configuration.defaultConfiguration.fileURL!)
        }
    }

    private func addCredentailGroupWithRealmSwiftUI() {
        // 그룹 만들면서 Credentail 까지 생성
        let groupC1 = Credential(title: "G)제목2",
                                 userName: "G)유저네임2",
                                 passwordKey: "G)키2",
                                 memo: "G)메모2")

        let list = RealmSwift.List<Credential>()
        list.append(groupC1)
        $credentailGroup.append(
            CredentialGroup(title: "그룹제목2",
                            sequence: 2,
                            credentials: list)
        )
        
        // 그룹만 만들기
        $credentailGroup.append(
            CredentialGroup(title: "그룹제목2",
                            sequence: 2)
        )
    }
    
    private func originRealmCRUD() {
        guard let first = credential.first else { return }
        
        do {
            let realm = try Realm()
            guard let firstCredentail = realm.object(ofType: Credential.self,
                                                     forPrimaryKey: first.id) else {
                print("wrong primaryKey")
                return
            }
            try realm.write {
                firstCredentail.title = "제목1 두번째 업데이트"
                realm.delete(firstCredentail)
            }
        } catch let e {
            print(e.localizedDescription)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct CredentialListView: View {
    @ObservedRealmObject var credentialGroup: CredentialGroup

    var leadingBarButton: AnyView?

    var body: some View {
        NavigationView {
            VStack {
                List {
                    ForEach(credentialGroup.credentials) { credentail in
                        CredentialItemRow(credential: credentail)
                    }
                    .onDelete(perform: $credentialGroup.credentials.remove)
                    .onMove(perform: $credentialGroup.credentials.move)
                }
                .listStyle(.grouped)
                .navigationBarTitle(
                    "Credentails",
                    displayMode: .large
                )
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(
                    leading: self.leadingBarButton,
                    trailing: EditButton()
                )
                
                HStack {
                    Spacer()
                    Button {
                        addCredential()
                    } label: {
                        Image(systemName: "plus")
                    }
                }
                .padding()
            }
        }
    }
    
    private func addCredential() {
        $credentialGroup.credentials.append(
            Credential(title: "G1)제목2",
                       userName: "G1)유저네임2",
                       passwordKey: "G1)키2",
                       memo: nil)
        )
    }
}

struct CredentialItemRow: View {
    @ObservedRealmObject var credential: Credential

    var body: some View {
        NavigationLink(destination: CredentialDetailsView(credential: credential)) {
            Text(credential.title)
        }
    }
}

struct CredentialDetailsView: View {
    @ObservedRealmObject var credential: Credential

    var body: some View {
        VStack(alignment: .leading) {
            Spacer()

            TextField("Insert New Create", text: $credential.created)

            TextField("Insert New Title", text: $credential.title)

            TextField("Insert New UserName", text: $credential.userName)

            TextField("Insert New PasswordKey", text: $credential.passwordKey)

            TextField("Insert New Meno", text: $credential.memo.toUnwrapped(defaultValue: ""))

            Spacer()
        }
        .padding()
        .navigationBarTitle(credential.title)
    }
}

extension Binding {
     func toUnwrapped<T>(defaultValue: T) -> Binding<T> where Value == Optional<T>  {
        Binding<T>(get: { self.wrappedValue ?? defaultValue }, set: { self.wrappedValue = $0 })
    }
}
