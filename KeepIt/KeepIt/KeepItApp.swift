//
//  KeepItApp.swift
//  KeepIt
//
//  Created by IMFORM-MM-2122 on 2022/10/26.
//

import SwiftUI

@main
struct KeepItApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
//            LocalOnlyContentView()
        }
    }
}
