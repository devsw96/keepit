//
//  DateUtils.swift
//  KeepIt
//
//  Created by imform-mm-2103 on 2022/10/26.
//

import Foundation

extension Date {
    func toFormat(dateFormat: String, localeIndetifier: String = "ko_KR") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.locale = Locale(identifier: localeIndetifier)
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = dateFormat
        return formatter.string(from: self)
    }
}
